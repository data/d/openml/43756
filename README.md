# OpenML dataset: Emotions--Sensor-Data-Set

https://www.openml.org/d/43756

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Emotions Detection Is an Interesting Blend of Psychology and Technology.
-This Technology Helps to Build a Companion Robots,This Robots Can be Friendly and Have The Ability to Recognize Users Emotions and Needs, and to Act Accordingly.
-Its Essentially a Way to Determine How your Consumers are Reacting to Your Website, Social Media Posts, and Other Forms of Your Online Content This Helps to Transform the Face of Marketing and Advertising by Reading Human Emotions and Then Adapting Consumer Experiences to These Emotions in Real Time.
1-What is Emotions Sensor Dataset:
Emotions Sensor Data Set Contain Top 23 730 English Words Classified Statistically Using Naive Bayes  Algorithm Into 7 Basic Emotion Disgust, Surprise ,Neutral ,Anger ,Sad ,Happy and Fear.
2-How We Build This Dataset:
-First We Collected Thousands of Sentences , Blogs and  Twitters .
all about  1.185.540  Words 
-We Labeled Manually and Automatically this Sentences  Into 7 Basic Emotion Disgust, Surprise ,Neutral ,Anger ,Sad ,Happy and Fear.
-Now We Choose The Top of Most Used  23 730  English Words
-Word by Word We Calculated The Probabilities of Existence of  This  Words in  Disgust, Surprise ,Neutral ,Anger ,Sad ,Happy and Fear Sentences and Put Them in Simple CSV File 
-We Used The Naive Bayes  Algorithm To Calculate This Probabilities Of Existence Of This Words
3-Where To Use This Dataset:
Emotions Sensor DataSet Helps To Detect Emotions In Text or Voice Speech and You Can Easily Build a Sentiment Analysis Bot In few simple steps.
Questions or Get the  full Emotions Sensor Data Set  Contain Top 23730 English Words ?
To Get your  copy of the  full Emotions Sensor Data Set    Or Contact me   : codibitsgmail.com

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43756) of an [OpenML dataset](https://www.openml.org/d/43756). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43756/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43756/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43756/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

